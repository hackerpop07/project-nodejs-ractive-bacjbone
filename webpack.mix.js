
const mix = require('laravel-mix');

const PATHS = {
    src: './resources',
    dist: './public'
};

mix.js(`${PATHS.src}/js/app.js`, `${PATHS.dist}/js`).webpackConfig({
	module: {
		rules: [
			{
				test: /\.mustache$/,
				use: 'raw-loader'
			},
		]
	}
});

mix.sass(`${PATHS.src}/sass/app.scss`, `${PATHS.dist}/css`);