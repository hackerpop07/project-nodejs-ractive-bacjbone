'use strict';

exports.name = 'http.app';

exports.requires = [
    '@express',
    '@path',
    '@cookie-parser',
    '@morgan',
    '@body-parser',
    '@cors',
    '@helmet',
    '@http-errors',
    '@mongoose',
    '@dotenv',
    'middlewares.errors-handle',
    'routes.index',
    'routes.api',
    'routes.userApi',
];

exports.factory = function (
    express,
    path,
    cookieParser,
    logger,
    bodyParser,
    cors,
    helmet,
    createError,
    mongoose,
    env,
    midErrorsHandle,
    indexRouter,
    apiRouter,
    userApiRouter) {

    env.config();

    mongoose.connect(process.env.MONGODB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    const app = express();

    app.use(logger('combined'));
    app.use(express.json());
    app.use(express.urlencoded({extended: false}));
    app.use(cookieParser());
    app.use(express.static('./public'));

    // Set view engine.
    app.set('views', path.join(path.resolve(__dirname, '../..'), 'views'));
    app.set('view engine', 'pug');

    // Using bodyParser to parse JSON bodies into JS objects
    app.use(bodyParser.urlencoded({extended: true}));

    // CORS.
    app.use(cors());

    // Adding Helmet to enhance your API's security
    app.use(helmet());

    // Register routers.
    app.use('/', indexRouter);
    app.use('/api/', apiRouter);
    app.use('/api/', userApiRouter);

    // Catch 404 and forward to error handler
    app.use(function (req, res, next) {
        next(createError(404));
    });

    // Handle errors
    app.use(midErrorsHandle);

    return app;
};
