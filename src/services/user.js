'use strict';

exports.name = 'services.user';

exports.requires = [
    'models.user'
];

exports.factory = function (model) {
    return {
        getAll: () => {
            return model.find({}, null, {}).limit(5);
        },
        store: (data) => {
            let contact = new model(data);
            return contact.save();
        },
        update: (data, id) => {
            return model.updateOne({_id: id}, data);
        },
        delete: (id) => {
            return model.findByIdAndRemove(id);
        },
        getUser: (id) => {
            return model.find({_id: id}, null, {});
        }
    }
};