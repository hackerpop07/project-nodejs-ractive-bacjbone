'use strict';

exports.name = "routes.userApi";

exports.requires = [
    '@express',
    'controller.userController'
];

exports.factory = function (express, userController) {
    let router = express.Router();

    router.get('/users/', userController.getAll);
    router.get('/users/:id', userController.getUser);
    router.post('/users', userController.store);
    router.delete('/users/:id', userController.delete);
    router.put('/users/:id', userController.update);

    return router;
};