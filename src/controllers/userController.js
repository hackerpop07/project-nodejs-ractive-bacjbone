'use strict';

exports.name = "controller.userController";

exports.requires = [
    'services.user'
];

exports.factory = function (serviceUser) {
    return {
        getAll: function (req, res, next) {
            serviceUser.getAll().then((users) => {
                res.json(users);
            }).catch((error) => {
                res.status(404).json({message: 'user not found'});
            })
        },
        store: function (req, res, next) {
            serviceUser.store(req.body).then((user) => {
                res.json({
                    msg: "store success",
                    user: user
                });
            }).catch((error) => {
                res.json({error: error});
            })
        },
        delete: function (req, res, next) {
            serviceUser.delete(req.params.id).then((user) => {
                res.json({
                    msg: "delete success",
                    user: user
                })
            }).catch((err) => {
                res.json({
                    msg: err
                })
            })
        },
        update: function (req, res, next) {
            serviceUser.update(req.body, req.params.id).then((user) => {
                res.json({
                    msg: "update success",
                    user: user
                })
            }).catch((err) => {
                res.json({msg: err});
            })
        },
        getUser: function (req, res, next) {
            serviceUser.getUser(req.params.id).then((user) => {
                res.json({user: user});
            }).catch((err) => {
                res.json({msg: err});
            })
        }
    }
};
