import { model } from "mongoose";

const Collection = require("../models/user/collection").default;
const Model = require("../models/user/model").default;

const UserService = {
    getAll: () => {
        return new Collection().fetch();
    },
    store: (params) => {
        return new Promise((resolve, reject) => {
            try {
                let user = new Collection().create(params)
                resolve(user);
            } catch (err) {
                reject(err);
            }
        });
    },
    delete: (id) => {
        let user = new Model({ _id: id });
        return user.destroy();
    },
    update: (data) => {
        let user = new Model(data);

        return user.save();
    }
}

export default UserService;