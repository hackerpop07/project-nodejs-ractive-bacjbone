
module.exports = {
    form: {
        addNew: {
            rules: {
                name: {
                    presence: true,
                    length: {
                        minimum: 6
                    }
                },
                address: {
                    presence: true,
                    length: {
                        minimum: 6
                    }
                },
                gender: {
                    presence: true,
                    length: {
                        minimum: 6
                    }
                }
            }
        }
    }
}