const Ractive = require("ractive").default;
const service = require("../../../services/user").default;
const validate = require("validate.js");
const config = require("../../../config/default");
const swal = require('sweetalert2');

module.exports = Ractive.extend({
    template: require("./edit.mustache").default.toString(),
    components: {
        'app-error': require('../../shared/validate/error/error')
    },
    data: {
        name: "",
        address: "",
        gender: ""
    },
    oncomplete: function () {
        let self = this;
        self.setUser();


    },
    setUser: function () {
        let user = this.get("user").user;
        this.set('name', user.name);
        this.set('address', user.address);
        this.set('gender', user.gender);
    },
    getData: function () {
        let self = this;

        return {
            name: self.get('name').trim(),
            address: self.get('address').trim(),
            gender: self.get('gender').trim()
        };
    },
    setData: function (user, data) {
        user.name = data.name;
        user.address = data.address;
        user.gender = data.gender;
    },
    on: {
        updateUser: function (ctx) {
            ctx.event.preventDefault();
            let self = this;
            let params = self.getData();
            let errors = validate(params, config.form.addNew.rules);
            let user = self.get("user");
            // vị trí của user trên local
            let index = user.index;
            params._id = user.user._id;

            console.log(errors);
            if (errors) {
                self.set('errors', errors);
            } else {
                service.update(params)
                    .then((result) => {
                        let users = self.parent.get("users");
                        self.setData(users[index], params);
                        self.parent.set("users", users);
                        swal.fire('Successful', 'A new user has been created', 'success').then(() => {
                            self.parent.set('page', 'Home');
                        });
                    }).catch((err) => {
                        swal.fire(err.statusText)
                    });
            }
            return false;
        }
    }
})