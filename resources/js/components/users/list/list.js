const Ractive = require("ractive").default;
const userService = require("../../../services/user").default;
const _ = require('lodash');
const swal = require("sweetalert2");
const editUserForm = require("../edit/edit");

module.exports = Ractive.extend({
    template: require("./list.mustache").default.toString(),
    data: {

    },
    components: {
        "app-edit-user-form": editUserForm
    },
    oncomplete: function () {
        let self = this;
        self.getAll();


    },
    on: {
        goto: function (ctx, page) {
            if (!page) {
                return;
            }
            this.parent.set('page', 'addUserForm');
            return false;
        }
    },
    getAll: function () {
        let self = this;

        userService.getAll().then((users) => {
            self.set("users", users);
        })
    },
    editUser: function (index, id) {
        let users = this.get("users");
        let user = this.get("user");
        user = {
            user: users[index],
            index: index
        }
        this.set('user', user);
        this.parent.set('page', 'editUserForm');

    },
    deleteUser: function (index, id) {
        let self = this;

        swal.fire({
            title: 'Are you sure?',
            text: 'You will not be able to recover this imaginary file!',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {

            if (result.value) {
                userService.delete(id)
                    .then((result) => {
                        //delete user
                        let users = self.get('users');
                        users.splice(index, 1);
                        self.set('users', users);
                    }).catch((err) => {
                        swal.fire(err.statusText)
                    });
            } else if (result.dismiss === swal.DismissReason.cancel) {
                swal.fire(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })
    }
})