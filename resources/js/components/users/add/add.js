
const Ractive = require("ractive").default;
const validate = require("validate.js");
const swal = require("sweetalert2");
const service = require("../../../services/user").default;
const config = require("../../../config/default");

module.exports = Ractive.extend({
    template: require("./add.mustache").default.toString(),
    components: {
        'app-error': require('../../shared/validate/error/error')
    },
    data: {
        name: "",
        address: "",
        gender: ""
    },
    on: {
        addUser: function (ctx) {
            ctx.event.preventDefault();
            let self = this;
            let params = self.getData();
            let errors = validate(params, config.form.addNew.rules);
            

            if (errors) {
                self.set('errors', errors);
            } else {
                service.store(params)
                    .then((result) => {
                        swal.fire('Successful', 'A new user has been created', 'success').then(() => {
                            self.parent.set('page', 'Home');
                        });
                    }).catch((err) => {
                        swal.fire(err.statusText)
                    });
            }
        }
    },
    getData: function () {
        let self = this;

        return {
            name: self.get('name').trim(),
            address: self.get('address').trim(),
            gender: self.get('gender').trim()
        };
    }
})