const Backbone = require("backbone");

export default Backbone.Model.extend({
    urlRoot: '/api/users/',
    idAttribute: '_id',
})
