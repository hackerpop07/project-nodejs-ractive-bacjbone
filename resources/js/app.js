
const Ractive = require("ractive").default;
const userList = require("./components/users/list/list");
const header = require("./components/header/header");
const footer = require("./components/footer/footer");
const addUserForm = require("./components/users/add/add");

new Ractive({
    target:"#application",
    template: require("./app.mustache").default.toString(),
    data:{
        page: PAGE,
        user: {
            name: 44444
        }
    },
    components:{
        "app-user-list": userList,
        "app-header": header,
        "app-footer": footer,
        "app-add-user-form": addUserForm,
    }
})